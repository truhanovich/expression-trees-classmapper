﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Sample2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var mapGenerator = new MappingGenerator();
            var mapper = mapGenerator.Generate<Foo, Bar>();

            var res = mapper.Map(new Foo {Age = 35, Name = "Alex"});
        }
    }
}
