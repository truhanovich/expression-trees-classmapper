﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Sample2
{
    public class Mapper<TSource, TDestination>
    {
        private readonly Func<TSource, TDestination> _mapFunction;

        internal Mapper(Func<TSource, TDestination> func)
        {
            _mapFunction = func;
        }

        public TDestination Map(TSource source)
        {
            var result = _mapFunction(source);
            var tDestinationProp = result.GetType().GetProperties();
            foreach (var prop in source.GetType().GetProperties())
            {
                var propertyInfo = tDestinationProp.SingleOrDefault(t => t.Name == prop.Name);
                propertyInfo?.SetValue(result, prop.GetValue(source));
            }
                return result;
        }
    }

    public class MappingGenerator
    {
        public Mapper<TSource, TDestination> Generate<TSource, TDestination>()
        {
            var sourceParam = Expression.Parameter(typeof (TSource));
            var mapFunction =
                Expression.Lambda<Func<TSource, TDestination>>(
                    Expression.New(typeof (TDestination)),
                    sourceParam
                    );
            return new Mapper<TSource, TDestination>(mapFunction.Compile());
        }
    }

    public class Foo
    {
        public int Age { get; set; }
        public string Name { get; set; }
    }

    public class Bar
    {
        public int Age { get; set; }
        public string Name { get; set; }
    }
}
